(function () {
    /** Initialize the FirebaseUI Widget using Firebase.
     * 
     * @type firebaseui.auth.AuthUI 
     * https://firebase.google.com/docs/auth/web/firebaseui
     * */

    var ui = new firebaseui.auth.AuthUI(firebase.auth());



    //https://firebase.google.com/docs/auth/web/firebaseui
    ui.start('#firebaseui-auth-container', {
        signInOptions: [
            firebase.auth.EmailAuthProvider.PROVIDER_ID
        ],
        // Other config options...
    });

    ui.start('#firebaseui-auth-container', {
        signInOptions: [
            {
                provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
                requireDisplayName: false
            }
        ]
    });





//!!! module ?? https://docs.angularjs.org/api/ng/function/angular.module
    var app = angular.module('myAppModule', ['ngRoute'])

//project.js
            .value('fbURL', 'https://lesmacdodenantes.firebaseio.com/')
            .service('fbRef', function (fbURL) {
                return new Firebase(fbURL)
            })
//            .service('fbAuth', function ($q, $firebase, $firebaseAuth, fbRef) {
//                var auth;
//                return function () {
//                    if (auth)
//                        return $q.when(auth);
//                    var authObj = $firebaseAuth(fbRef);
//                    if (authObj.$getAuth()) {
//                        return $q.when(auth = authObj.$getAuth());
//                    }
//                    var deferred = $q.defer();
//                    authObj.$authAnonymously().then(function (authData) {
//                        auth = authData;
//                        deferred.resolve(authData);
//                    });
//                    return deferred.promise;
//                }
//            })

            .service('Projects', function ($q, $firebase, fbRef, fbAuth, projectListValue) {
                var self = this;
                this.fetch = function () {
                    if (this.projects)
                        return $q.when(this.projects);
                    return fbAuth().then(function (auth) {
                        var deferred = $q.defer();
                        var ref = fbRef.child('projects-fresh/' + auth.auth.uid);
                        var $projects = $firebase(ref);
                        ref.on('value', function (snapshot) {
                            if (snapshot.val() === null) {
                                $projects.$set(projectListValue);
                            }
                            self.projects = $projects.$asArray();
                            deferred.resolve(self.projects);
                        });

                        //Remove projects list when no longer needed.
                        ref.onDisconnect().remove();
                        return deferred.promise;
                    });
                };
            })


            .controller('myController', ['$scope', function ($scope) {

                    $scope.count1 = 0;
                    $scope.count2 = 0;
                    $scope.count3 = 0;
                    $scope.count4 = 0;
                    $scope.vall = 999;
                    $scope.showManualRestosBool = false;
                    $scope.nbClicks = 0;

                    $scope.change1 = function () {
                        $scope.count1++;
                    };
                    $scope.change2 = function () {
                        $scope.count2++;
                    };
                    $scope.change3 = function () {
                        $scope.count3++;
                    };
                    $scope.change4 = function () {
                        $scope.count4++;
                    };



//https://firebase.google.com/docs/database/web/start?authuser=0
// Set the configuration for your app
                    // TODO: Replace with your project's config object
//                    var config = {
//                        apiKey: "apiKey",
//                        authDomain: "lesmacdodenantes.firebaseapp.com",
//                        databaseURL: "https://lesmacdodenantes.firebaseio.com/",
//                        storageBucket: "bucket.appspot.com"
//                    };
//                    firebase.initializeApp(config);

                    // Get a reference to the database service
                    //var database = firebase.database();





//https://firebase.google.com/docs/database/web/read-and-write?authuser=0
                    $scope.writeUserDataa = function writeUserData() {
                        console.log("write data");
                        firebase.database().ref('tels/' + "02 40 43 10 77").set({
                            lat: 140,
                            long: 90,
                            open: 8,
                            close: 0,
                            address: "15 rue de Saint Nazaire C.C Casino 44800 SAINT HERBLAIN"
                        });
                        firebase.database().ref('tels/' + "02 40 92 88 46").set({
                            lat: 140,
                            long: 90,
                            open: 8,
                            close: 0,
                            address: 'McDonalds Centre commercial Atlantis 2.6 km � ZAC du Moulin Neuf, Rue Oc�ane'
                        });
                    }


                    //TODO config to Github env var
                    //TODO externalize to Firebase function
                    //Google maps API
                    $scope.address2lat = function (addressString) {
                        return null;
                    };

                    $scope.address2long = function () {
                        //internal function
                    };

                    $scope.address2latlongPrototype = function () {
                        //internal function
                    };

                    $scope.del = function deleteAll() {
                        //internal function
                    };

                    $scope.add1 = function (val) {
                        return val + 1;
                    };




                    $scope.readd = function () {

                        //https://firebase.google.com/docs/database/web/read-and-write?authuser=0 
                        //  $scope.user = firebase.auth().currentUser.uid;
//                       
//                         return firebase.database().ref('/tels/').once('value').then(function (snapshot) {
//                            //TODO  && ???
//                            //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
//                            snapshot.val();
//                        });
                        console.log('in readd');
                        //var telss = firebase.database().ref('tels/');
                        var telss = firebase.database().ref('/');
                        telss.on('value', function (snapshot) {
                            $scope.snap = snapshot;
                            console.log('Time' + new Date().toUTCString() + 'in on');

                            if (snapshot === null) {
                                console.log('snap null');
                            } else {
                                console.log('snap not null');
                                $scope.telssJson = snapshot.val();
                               // $scope.telssObj = JSON.parse($scope.telssJson);
                                console.log('dataNew0=' + JSON.stringify(snapshot.val()));
                                //console.log('dataNew=' + $scope.telssJson);
                                //console.log('snapp2=' + snapshot);
                                //console.log('snapp3=' + snapshot.val());
                                // console.log('snapp4=' + snapshot.text);
//TODO log this read into the database
                                console.log('scope.telssJson.lesmacdodenantes.tels.t0240884411.close' + $scope.telssJson.lesmacdodenantes.tels.t0240884411.close);
                              
                                // console.log('telssObj=' + $scope.telssObj.lesmacdodenantes.tels.t0240884411.close);
                                

                            }
                        });

                        return telss;
                    };

                    $scope.showManualRestos = function () {
                        $scope.showManualRestosBool = !$scope.showManualRestosBool;
                        $scope.nbClicks++;
                        console.log('showManualRestosBool');
                        console.log('showManualRestosBool=' + $scope.showManualRestosBool);
                        console.log('nbclicks=' + $scope.nbClicks);
                    };
                }])

            .directive('includeRestoManual', function () {
                return  {restrict: 'E',
                    templateUrl: 'include-restos.html'
                };
            });
    console.log('in controller');
})();